var colors = [];
colors = randomGen(6);
console.log(colors);

var colorBox = document.querySelectorAll(".clr-set");
var rbgTxt = document.querySelector("#rgb-clr");
var header = document.querySelector("#main-head");
var statbar = document.querySelector("#status");
var btnClr = document.querySelector("#btn-clr");
var btn1 = document.querySelector("#btn1");
var btn2 = document.querySelector("#btn2");
var parent = document.querySelector("#clr-cntr");

var gameWon = false;
var easyMode = false;

var pickedColor = pickRandomColor();
rbgTxt.textContent = pickedColor.toUpperCase();

btn1.addEventListener("click",function(e){
    btn1.classList.add("btn-toggle");
    btn2.classList.remove("btn-toggle");
    colors = randomGen(3);
    pickedColor = pickRandomColor();
    rbgTxt.textContent = pickedColor.toUpperCase();
    for (var i = 0; i < colorBox.length; i++) {
        colorBox[i].style.backgroundColor = colors[i];
    }
    for(var i=3; i<6;i++){
        colorBox[i].style.backgroundColor = "#232323";
        colorBox[i].classList.remove("clr-set");
    }
    easyMode = true;
    resetGame();
});
btn2.addEventListener("click",function(e){
    easyMode = false;
    btn2.classList.add("btn-toggle");
    btn1.classList.remove("btn-toggle");
    colors = randomGen(6);
    pickedColor = pickRandomColor();
    rbgTxt.textContent = pickedColor.toUpperCase();
    for(var i=3; i<6;i++){
        colorBox[i].classList.add("clr-set");
    }
    for (var i = 0; i < colorBox.length; i++) {
        colorBox[i].style.backgroundColor = colors[i];
    }
    resetGame();
});


btnClr.addEventListener("click", function(e){
    if(easyMode){
        colors = randomGen(3);
    }else{
        colors = randomGen(6);
    }
    pickedColor = pickRandomColor();
    rbgTxt.textContent = pickedColor.toUpperCase();
    for (var i = 0; i < colorBox.length; i++) {
        colorBox[i].style.backgroundColor = colors[i];
    }
   resetGame();
});

for (var i = 0; i < colorBox.length; i++) {
    colorBox[i].style.backgroundColor = colors[i];
    colorBox[i].addEventListener("click", function (e) {
        console.log(pickedColor + ":" + this.style.backgroundColor);
        if (this.style.backgroundColor === pickedColor) {
            header.style.backgroundColor = this.style.backgroundColor;
            statbar.textContent = "You win";
            btnClr.textContent = "Play again?";
            gameWon = true;
            colorBox.forEach(element => {
                element.style.backgroundColor = this.style.backgroundColor;
            });
        }else{
           this.style.backgroundColor = "#232323";
           statbar.textContent = "Try again";
        }
    });
}


function randomGen(num) {
    var colors =[];
    for (var i = 0; i < num; i++) {
        var clrRed = Math.floor(Math.random() * 256);
        var clrGreen = Math.floor(Math.random() * 256);
        var clrBlue = Math.floor(Math.random() * 256);
        colors[i] = "rgb(" + clrRed + ", " + clrGreen + ", " + clrBlue + ")";
    }

    return colors;
}

function pickRandomColor(){
    var randNum = Math.floor(Math.random() * (colors.length));
    return colors[randNum];
}

function resetGame(){
    header.style.backgroundColor = "steelblue";
    btnClr.textContent = "New Colors";
    statbar.textContent = "";
}